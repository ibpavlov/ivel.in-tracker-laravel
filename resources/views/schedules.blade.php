@extends('layouts.app')

@section('content')
	{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('item_id', 'Item_id:') !!}
			{!! Form::text('item_id') !!}
		</li>
		<li>
			{!! Form::label('frequency', 'Frequency:') !!}
			{!! Form::text('frequency') !!}
		</li>
		<li>
			{!! Form::label('offset', 'Offset:') !!}
			{!! Form::text('offset') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}
@endsection