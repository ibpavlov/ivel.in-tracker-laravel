<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchedulesTable extends Migration {

	public function up()
	{
		Schema::create('schedules', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('item_id')->unsigned();
			$table->timestamps();
			$table->string('frequency', 50)->index()->default('daily');
			$table->integer('offset')->unsigned()->index()->default('0');
		});
	}

	public function down()
	{
		Schema::drop('schedules');
	}
}