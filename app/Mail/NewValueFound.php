<?php

namespace App\Mail;

use App\Models\Value;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewValueFound extends Mailable
{
    use Queueable, SerializesModels;

    /** @var Value */
    private $value;

    /**
     * Create a new message instance.
     *
     * @param Value $value
     */
    public function __construct(Value $value)
    {
        $this->value = $value;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $item = $this->value->item;
        $this->subject(__("[NEW]") . " " . $item->title);
        return $this->markdown('notification.newValueFound', ['value' => $this->value, 'item' => $item]);
    }
}
