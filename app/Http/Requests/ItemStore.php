<?php
/**
 * File for ItemUpdateRequest
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ItemStore extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'url'   => 'required',
            'title' => 'required',
            'frequency' => 'required',
            'via' => 'required'
        ];
    }
}