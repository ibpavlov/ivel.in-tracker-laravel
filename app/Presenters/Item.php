<?php
/**
 * File for Item
 */

namespace App\Presenters;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\HtmlString;
use McCool\LaravelAutoPresenter\BasePresenter;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

/**
 * Class Item
 * @package App\Presenters
 * @property \App\Models\Item $wrappedObject
 */
class Item extends BasePresenter
{
    /**
     * @return string
     */
    public function formattedValue()
    {
        return preg_replace(
            [
                "/\n+([\t\f\v ]+\n+)+/m",
                "/\n+/m",
                "/[\r\n\t\f\v ]+/"
            ], [
                "\n",
                "\n",
                " "
            ],
            $this->wrappedObject->value ?? ""
        );
    }

    /**
     * @return string
     */
    public function markdownValue()
    {
        return preg_replace(
            [
                "/\n+([\t\f\v ]+\n+)+/m",
                "/\n\n+/m"
            ], [
            "\n",
            "\n\n"
        ],
            $this->wrappedObject->value ?? ""
        );
    }

    /**
     * @return string
     */
    public function limitedValue()
    {
        return str_limit($this->formattedValue() ?? 'Not defined', 100);
    }

    /**
     * @return string
     */
    public function limitedUrl()
    {
        return str_limit($this->wrappedObject->url ?? 'Not defined', 200);
    }

    /**
     * @return string
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function screenshotThunb()
    {
        $thumb = preg_replace('/(\.gif|\.jpg|\.png)/', '_thumb$1', $this->wrappedObject->screenshot);
        if(!is_file($thumb)) {
            Image::load($this->wrappedObject->screenshot)
                ->fit(Manipulations::FIT_FILL, 300, 200)
                ->save($thumb);
        }
        return Storage::url($thumb);
    }

    /**
     * @return string
     */
    public function screenshotUrl()
    {
        return Storage::url($this->wrappedObject->screenshot);
    }

    public function html()
    {
        return new HtmlString($this->wrappedObject->valueHtml);
    }
}