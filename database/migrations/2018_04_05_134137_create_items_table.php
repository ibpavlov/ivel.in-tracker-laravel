<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	public function up()
	{
		Schema::create('items', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('user_id')->unsigned();
			$table->timestamps();
            $table->softDeletes();
            $table->string('url', 2000);
            $table->string('publicUrl', 2000)->nullable();
			$table->string('domain', 255)->nullable();
			$table->text('selector')->nullable();
			$table->text('instructions')->nullable();
            $table->text('value')->nullable();
            $table->text('valueHtml')->nullable();
            $table->string('title', 1000);
            $table->string('screenshot', 1000)->nullable();
		});
	}

	public function down()
	{
		Schema::drop('items');
	}
}