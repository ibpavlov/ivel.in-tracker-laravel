<?php

namespace App\Notifications;

use App\Models\Item;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;

abstract class DbNotification extends Notification
{
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\NotificationChannel[] */
    protected $notificationChannels;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(Item $notifiable)
    {
        $this->notificationChannels = $notifiable->notificationChannels;
        return array_merge($this->notificationChannels->pluck('via')->toArray(), ['database']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  Model|Notifiable|DbNotification $notifiable
     * @return array
     */
    abstract public function toArray($notifiable);
}
