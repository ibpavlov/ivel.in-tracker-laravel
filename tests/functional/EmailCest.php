<?php

use App\Mail\NewValueFound;
use App\Models\Item;
use App\Models\Value;
use Illuminate\Support\Facades\Mail;

class EmailCest
{

    public function email(FunctionalTester $I)
    {
        $user = factory(\App\Models\User::class)->create([
            'email' => 'ibpavlov@gmail.com'
        ]);
        /** @var Item $item */
        $item = factory(Item::class)->create(['user_id' => $user->id]);
        $value = factory(Value::class)->create(['item_id' => $item->id]);
        Mail::to($item->user)->sendNow(new NewValueFound($value));
        $I->assertEmpty(Mail::failures());
    }

}
