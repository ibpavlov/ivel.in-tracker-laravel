<?php

namespace App\Policies;

use App\Models;
use Illuminate\Auth\Access\HandlesAuthorization;

class Item
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the item.
     *
     * @param  Models\User  $user
     * @param  Models\Item  $item
     * @return mixed
     */
    public function view(Models\User $user, Models\Item $item)
    {
        return true;
    }

    /**
     * Determine whether the user can create items.
     *
     * @param  Models\User  $user
     * @return mixed
     */
    public function create(Models\User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the item.
     *
     * @param  Models\User  $user
     * @param  Models\Item  $item
     * @return mixed
     */
    public function update(Models\User $user, Models\Item $item)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the item.
     *
     * @param  Models\User  $user
     * @param  Models\Item  $item
     * @return mixed
     */
    public function delete(Models\User $user, Models\Item $item)
    {
        return true;
    }
}
