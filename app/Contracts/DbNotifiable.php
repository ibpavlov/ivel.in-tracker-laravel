<?php
/**
 * File for DbNotifiable
 */

namespace App\Contracts;


use App\Models\User;

interface DbNotifiable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notificationChannels();

    /**
     * @return User|string|null
     */
    public function emailTo();
}