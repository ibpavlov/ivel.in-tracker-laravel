<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationChannel extends Model
{
    const TYPE_EMAIL = 'mail';

    const TYPES = [
        self::TYPE_EMAIL
    ];

    protected $table = 'notification_channels';
    public $timestamps = true;
    protected $fillable = array('via', 'item_id', 'extra', 'subject', 'message');

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    /**
     * Scope a query to only include email channels only
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEmails($query)
    {
        return $query->where('via', 'mail');
    }

}