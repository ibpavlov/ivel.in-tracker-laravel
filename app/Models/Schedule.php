<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use Scopes\Schedule;

    const FREQUENCY_MINUTELY = 'minutely';
    const FREQUENCY_HOURLY = 'hourly';
    const FREQUENCY_DAILY = 'daily';
    const FREQUENCY_WEEKLY = 'weekly';
    const FREQUENCY_MONTHLY = 'monthly';

    const FREQUENCIES = [
        self::FREQUENCY_MINUTELY,
        self::FREQUENCY_HOURLY,
        self::FREQUENCY_DAILY,
        self::FREQUENCY_WEEKLY,
        self::FREQUENCY_MONTHLY
    ];

    protected $table = 'schedules';
    public $timestamps = true;
    protected $fillable = ['frequency', 'offset', 'item_id'];

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

}