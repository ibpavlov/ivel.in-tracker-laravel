<?php

namespace App\Notifications;

use App\Models\Item;
use App\Models\Value;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;

class NewValueFound extends DbNotification
{
    use Queueable;

    /** @var Value */
    private $value;

    /**
     * Create a new notification instance.
     *
     * @param Value $value
     */
    public function __construct(Value $value)
    {
        $this->value = $value;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  Item $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage|Mailable
     */
    public function toMail(Item $notifiable)
    {
        return (new \App\Mail\NewValueFound($this->value))->to($notifiable->emailTo());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  Item $notifiable
     * @return array
     */
    public function toArray($notifiable) //todo Item
    {
        return [
            'for'      => $notifiable->getTable() . " #" . $notifiable->getKey(),
            'value id' => $this->value->id,
            'new'      => $this->value->value,
        ];
    }
}
