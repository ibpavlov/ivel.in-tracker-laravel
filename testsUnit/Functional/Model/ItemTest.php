<?php
/**
 * File for ItemTest
 */

namespace TestsUnit\Functional\Model;

use App\Models\Item;
use App\Models\Schedule;
use App\Models\Value;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TestsUnit\TestCase;

class ItemTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    public function testValues()
    {
        $item = factory(Item::class)->create();
        factory(Value::class)->create(['item_id' => $item->id]);
        $this->assertGreaterThan(0, $item->values()->count());
        $this->assertGreaterThan(0, $item->values->count());
    }

    public function testScheduled()
    {
        factory(Schedule::class)->create([
            'frequency' => Schedule::FREQUENCY_MINUTELY
        ]);
        $items = Item::scheduled()->count();
        $this->assertGreaterThan(0, $items);
    }

    public function testScheduledHourly()
    {
        factory(Schedule::class)->create([
            'frequency' => Schedule::FREQUENCY_HOURLY,
            'offset'    => date('i')
        ]);
        $items = Item::scheduled()->count();
        $this->assertGreaterThan(0, $items);
    }

    public function testScopeScheduled()
    {
        $this->assertTrue(true);
    }

    public function testSchedules()
    {
        $this->assertTrue(true);
    }

    public function testNotificationChannels()
    {
        $this->assertTrue(true);
    }
}
