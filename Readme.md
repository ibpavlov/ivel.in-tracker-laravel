# Tracker
This is a web platform for tracking website changes. You can use it for: 
- Price alert
- New Properties notification
- and etc.   

# Logic
It works by checking a page on schedule for specific elements on the page and sends notification when a change is noticed.

## Domain
- Page - Webpage that returns HTML or jSON content (You need to provide the url)
- Schedule - Monthly, Weekly, Daily, Hourly and Minutely frequency with ability to add an offset
- Page elements - You need to add css/xPath/jSonPath selector to target specific blocks on the page
- Notifications - Notifies you for a change in element - Email for now, but could be easily extended with System, Push, Sms, Slack and etc.

## Requirements
- Composer
- PHP > 7.1.3
- Mysql 

## Installation
- `composer install`
- `php artisan migrate`

## Getting started
1. `php artisan serve`
2. Open the website register and add your items for tracking 
3. Add cron jobs:
    - Windows - Import [storage\TrackerSchedulerTask.xml](storage/TrackerSchedulerTask.xml) into Task Manager and start the task
    - Linux - `* * * * * php {PATH_TO_ARTISAN}/artisan schedule:run` 
  
## TODO: 
- use https://github.com/jmespath/jmespath.php
- make screenshots of HTML pages
- different notifications
