<?php

namespace App\Console\Commands;

use App\Models\Item;
use App\Services\ItemTrack;
use Illuminate\Console\Command;

class Track extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'track:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Track all items and run schedules';

    /** @var ItemTrack */
    private $itemTrack;

    /**
     * Create a new command instance.
     *
     * @param ItemTrack $itemTrack
     */
    public function __construct(ItemTrack $itemTrack)
    {
        $this->itemTrack = $itemTrack;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Item::scheduled()->get()->each(function($item) {
            $this->itemTrack->track($item);
        });
        return 0;
    }

}
