<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function() {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/item/{item}/notify', 'ItemController@notify')->name('item.notify');
    Route::get('/item/{item}/reset', 'ItemController@reset')->name('item.reset');
    Route::get('/item/{item}/track', 'ItemController@track')->name('item.track');
    Route::get('/item/{item}/notifications', 'ItemController@notifications')->name('item.notifications');
    Route::get('/item/{item}/notifications/{notification}/read', 'ItemController@notificationRead')->name('item.notifications.read');

    Route::resources([
        'schedule' => 'ScheduleController',
        'item' => 'ItemController',
        'value' => 'ValueController',
        'notification' => 'NotificationController',
    ]);
});


