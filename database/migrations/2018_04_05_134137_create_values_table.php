<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateValuesTable extends Migration {

	public function up()
	{
		Schema::create('values', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->text('value');
			$table->integer('item_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('values');
	}
}