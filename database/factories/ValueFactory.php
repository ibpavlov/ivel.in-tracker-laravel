<?php

use App\Models\Item;
use App\Models\Value;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Value::class, function (Faker $faker) {
    return [
        'item_id' => factory(Item::class)->create()->id,
        'value' => $faker->randomNumber(5),
    ];
});
