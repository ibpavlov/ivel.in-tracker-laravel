<?php
/**
 * File for Item
 */

namespace App\Services;


use App\Http\Requests;
use \App\Models\Item;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ItemUpdate
 * @package App\Services
 */
class ItemUpdate
{
    /**
     * @param Item $item
     * @param Requests\ItemUpdate $request
     * @return Item
     */
    public function update(Item $item, Requests\ItemUpdate $request)
    {
        $item->update($request->all());
        $this->updateHasMany($item->schedules(), $request);
        $this->updateHasMany($item->notificationChannels(), $request);
        return $item;
    }

    /**
     * @param HasMany $relation
     * @param FormRequest $request
     */
    private function updateHasMany(HasMany $relation, FormRequest $request)
    {
        $get = $relation
            ->firstOrNew([
                $relation->getForeignKeyName() => $relation->getParentKey() //workaround for using with relation
            ]);
        $get->fill($request->only($relation->getModel()->getFillable()))
            ->save();
    }

}