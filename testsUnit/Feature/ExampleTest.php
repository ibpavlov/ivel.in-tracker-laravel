<?php

namespace TestsUnit\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use TestsUnit\TestCase;

class ExampleTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
