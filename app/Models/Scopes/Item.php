<?php
/**
 * File for Item
 */

namespace App\Models\Scopes;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

trait Item
{
    /**
     * Scope a query to only include scheduled items
     *
     * @param Builder $query
     * @param Carbon|null $time
     * @return Builder
     */
    public function scopeScheduled(Builder $query, Carbon $time = null)
    {
        return $query->whereHas('schedules', function (Builder $query) use ($time) {
            return $query->scheduled($time);
        });
    }

    /**
     * Scope a query to only items for authenticated user
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOwn(Builder $query)
    {
        return $query->where('user_id', Auth::id());
    }
}