<?php
/**
 * File for Item
 */

namespace App\Services;


use App\Http\Requests;
use App\Models\Item;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ItemStore
 * @package App\Services
 */
class ItemStore
{

    /**
     * @param Requests\ItemStore $request
     * @return Item
     */
    public function create(Requests\ItemStore $request)
    {
        $item = new Item();
        $item->setAttribute('user_id', $request->user()->id);
        $item->fill($request->only($item->getFillable()));
        $item->save();
        $this->createHasMany($item->schedules(), $request, 'frequency');
        $this->createHasMany($item->notificationChannels(), $request, 'via');
        return $item;
    }

    /**
     * @param HasMany $relation
     * @param FormRequest $request
     * @param string $relationColumn
     */
    private function createHasMany(HasMany $relation, FormRequest $request, string $relationColumn)
    {
        if($request->has($relationColumn)) {
            $relation->create($request->only($relation->getModel()->getFillable()));
        }
    }

}