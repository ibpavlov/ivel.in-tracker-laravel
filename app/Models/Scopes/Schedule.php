<?php
/**
 * File for Schedule
 */

namespace App\Models\Scopes;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait Schedule
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Carbon|null $time
     * @return mixed
     */
    public function scopeScheduled(Builder $query, Carbon $time = null)
    {
        if (!isset($time)) {
            $time = Carbon::now();
        }
        return $query->where("frequency", "minutely")
            ->orWhere("frequency", "hourly")
            ->where("offset", $time->minute)
            ->orWhere("frequency", "daily")
            ->where("offset", $time->minute + $time->hour * 60)
            ->orWhere("frequency", "weekly")
            ->where("offset", $time->minute + $time->hour * 60 + $time->dayOfWeek * 60 * 24)
            ->orWhere("frequency", "monthly")
            ->where("offset", $time->minute + $time->hour * 60 + $time->day * 60 * 24);
    }
}