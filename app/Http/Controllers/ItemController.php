<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Value;
use App\Services;
use App\Http\Requests;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Response;

class ItemController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Item::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Item::own()->latest()->get();

        return view('items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\ItemStore $request
     * @param Services\ItemStore $item
     * @return Response
     */
    public function store(Requests\ItemStore $request, Services\ItemStore $itemService)
    {
        $item = $itemService->create($request);
        return redirect()->route('item.show', compact('item'))
            ->with('success', 'Item created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  Item $item
     * @return Response
     */
    public function show(Item $item)
    {
        $schedule = $item->schedules()->first();
        $notificationChannel = $item->notificationChannels()->first();
        return view('items.edit', compact('item', 'schedule', 'notificationChannel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Item $item
     * @return Response
     */
    public function edit(Item $item)
    {
        $this->authorizeResource($item);
        return view('items.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Services\ItemUpdate $itemService
     * @param Requests\ItemUpdate $request
     * @param Item $item
     * @return Response
     */
    public function update(Requests\ItemUpdate $request, Services\ItemUpdate $itemService, Item $item)
    {
        $item = $itemService->update($item, $request);

        return redirect()->route('item.show', compact('item'))
            ->with('success', 'Item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Item $item
     * @return Response
     * @throws \Exception
     */
    public function destroy(Item $item)
    {
        $item->delete();

        return redirect()->route('item.index')
            ->with('success', 'Item deleted successfully');

    }

    /**
     * @param Item $item
     * @return mixed
     */
    public function notify(Item $item)
    {
        $this->authorizeResource($item, 'show');
        /** @var Value $value */
        $value = $item->values()->latest()->first();
        if (!isset($value)) {
            throw new ModelNotFoundException("This item has no values yet");
        }
        $item->notifyNow(new \App\Notifications\NewValueFound($value));

        return redirect()->route('item.notifications', compact('item'))
            ->with('success', 'Value Found notification sent successfully');
    }

    /**
     * @param Item $item
     * @return mixed
     */
    public function reset(Item $item)
    {
        $this->authorizeResource($item, 'update');
        $item->value = null;
        $item->valueHtml = null;
        $item->screenshot = null;
        $item->values()->delete();
        $item->save();

        return redirect()->route('item.show', compact('item'))
            ->with('success', __('Values are cleared'));
    }

    /**
     * @param Item $item
     * @param Services\ItemTrack $itemTrack
     * @return mixed
     * @throws \Exception
     */
    public function track(Item $item, Services\ItemTrack $itemTrack)
    {
        $this->authorizeResource($item, 'show');
        try {
            $itemTrack->track($item);
        } catch (\Exception $e) {
            return redirect()->route('item.show', compact('item'))
                ->with('error', $e->getMessage());
        }
        return redirect()->route('item.show', compact('item'))
            ->with('success', 'Value Found');
    }

    /**
     * @param Item $item
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function notifications(Item $item)
    {
        $notifications = $item->unreadNotifications()->get();
        return view('items.notifications', compact('notifications', 'item'));
    }

    /**
     * @param Item $item
     * @param DatabaseNotification $notification
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function notificationsRead(Item $item, DatabaseNotification $notification)
    {
        $this->authorizeResource($notification->notifiable(), 'show');
        $notification->markAsRead();
        return view('items.notifications', compact('notifications', 'item'));
    }
}

?>