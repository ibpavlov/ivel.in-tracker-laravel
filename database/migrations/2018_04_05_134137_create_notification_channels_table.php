<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationChannelsTable extends Migration {

	public function up()
	{
		Schema::create('notification_channels', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('via', 100)->default('mail');
			$table->integer('item_id')->unsigned();
			$table->text('extra')->nullable();
			$table->string('subject', 255)->nullable();
			$table->text('message')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('notification_channels');
	}
}