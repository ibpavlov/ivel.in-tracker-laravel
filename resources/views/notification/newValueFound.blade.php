@component('mail::message')
# New Value found
{{ $item->markdownValue }}

# Old value
{{ $value->previousValue }}

Value found for item {{ $item->title }} at {{ $item->domain }}

@component('mail::button', ['url' => $item->publicUrl ?? $item->url])
    Tracked page
@endcomponent

@component('mail::button', ['url' => route('item.show', [$item])])
    Item Details
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
