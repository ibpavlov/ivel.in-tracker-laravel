<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
        Schema::table('items', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
		Schema::table('schedules', function(Blueprint $table) {
			$table->foreign('item_id')->references('id')->on('items')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('values', function(Blueprint $table) {
			$table->foreign('item_id')->references('id')->on('items')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
        Schema::table('notification_channels', function(Blueprint $table) {
            $table->foreign('item_id')->references('id')->on('items')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
	}

	public function down()
	{
        Schema::table('items', function(Blueprint $table) {
            $table->dropForeign('items_user_id_foreign');
        });
		Schema::table('schedules', function(Blueprint $table) {
			$table->dropForeign('schedules_item_id_foreign');
		});
		Schema::table('values', function(Blueprint $table) {
			$table->dropForeign('values_item_id_foreign');
		});
		Schema::table('notification_channels', function(Blueprint $table) {
			$table->dropForeign('notification_channels_item_id_foreign');
		});
	}
}