<?php
/**
 * File for EmailNewValueFoundTest
 */

namespace TestsUnit\Functional;

use App\Mail\NewValueFound;
use App\Models\Item;
use App\Models\User;
use App\Models\Value;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Mail;
use TestsUnit\TestCase;

class EmailNewValueFoundTest extends TestCase
{
    use DatabaseMigrations;

    public function testEmail()
    {
        $user = factory(User::class)->create([
            'email' => 'ibpavlov@gmail.com'
        ]);
        /** @var Item $item */
        $item = factory(Item::class)->create(['user_id' => $user->id]);
        $value = factory(Value::class)->create(['item_id' => $item->id]);
        Mail::to($item->user)->sendNow(new NewValueFound($value));
        $this->assertEmpty(Mail::failures());
    }
}
