<?php

namespace App\Models\Traits;

use App\Models\NotificationChannel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait DbNotifiable
 * @mixin Model
 * @package App\Models\Traits
 */
trait DbNotifiable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notificationChannels()
    {
        return $this->hasMany(NotificationChannel::class, 'item_id');
    }

    /**
     * @return mixed
     */
    public function emailTo()
    {
        $emailChannel = $this->notificationChannels()->emails()->first(['extra']);
        if (!filter_var($emailChannel->extra ?? '', FILTER_VALIDATE_EMAIL)) {
            return $emailChannel->extra;
        }
        return $this->user;
    }
}
