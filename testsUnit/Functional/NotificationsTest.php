<?php

namespace TestsUnit\Functional;

use App\Models\Item;
use App\Models\NotificationChannel;
use App\Models\Value;
use App\Notifications\NewValueFound;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use TestsUnit\TestCase;

class NotificationTest extends TestCase
{
    use DatabaseMigrations;

    public function testBasicTest()
    {
        /** @var Item $item */
        $item = factory(Item::class)->create();
        $value = factory(Value::class)->create(['item_id' => $item->id]);
        factory(NotificationChannel::class)->create([
            'item_id' => $item->id,
            'via' => 'mail'
        ]);
        $item->notifyNow(new NewValueFound($value));
        $this->assertTrue(true);
    }
}
