<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Presenters;
use McCool\LaravelAutoPresenter\HasPresenter;

class Value extends Model implements HasPresenter
{

    protected $table = 'values';
    public $timestamps = true;
    protected $fillable = array('value', 'item_id');


    public function getPresenterClass()
    {
        return Presenters\Value::class;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|Model|\Illuminate\Database\Query\Builder|null|Value
     */
    public function previous()
    {
        return self::query()
            ->whereDate('created_at', '<', $this->getCreatedAtColumn())
            ->where('item_id', $this->item_id)
            ->latest()->first();
    }

}