<?php
/**
 * File for Value
 */

namespace App\Presenters;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\HtmlString;
use McCool\LaravelAutoPresenter\BasePresenter;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

/**
 * Class Value
 * @package App\Presenters
 * @property \App\Models\Value $wrappedObject
 */
class Value extends BasePresenter
{
    /**
     * @return string
     */
    public function previousValue()
    {
        return optional($this->wrappedObject->previous())->value ?? "Not found!";
    }
}