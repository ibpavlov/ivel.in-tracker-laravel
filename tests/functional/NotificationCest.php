<?php
namespace Tests\Functional;

use App\Models\Item;
use App\Models\NotificationChannel;
use App\Models\Value;

class NotificationCest
{

    public function send(\FunctionalTester $I)
    {
        /** @var Item $item */
        $item = factory(Item::class)->create();
        $value = factory(Value::class)->create(['item_id' => $item->id]);
        $notification = factory(NotificationChannel::class)->create([
            'item_id' => $item->id,
            'via' => 'mail'
        ]);
        $item->notifyNow(new \App\Notifications\NewValueFound($value));
        $I->assertTrue(true);
    }

}
