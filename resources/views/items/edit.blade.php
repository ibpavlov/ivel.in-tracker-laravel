@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-lg-2">
                @include('items.partial.form')
                <div class="clearfix"></div>
                {{ bs()->openForm('DELETE', route( 'item.destroy', $item)) }}
                {{ bs()->button("Delete")->addClass('offset-sm-2') }}
                {{ bs()->closeForm() }}

                {{ link_to_route('item.reset', 'Reset', [$item]) }}<br />
                {{ link_to_route('item.track', 'Track', [$item]) }}<br />
                {{ link_to_route('item.notify', 'Notify', [$item]) }}<br />
                {{ link_to_route('item.notifications', 'Notifications', [$item]) }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 offset-lg-2">
                @isset($item->value)
                    <div class="row">
                        <label class="col-sm-2">Value</label>
                        <div class="col-sm-10">
                            <pre>{{ $item->value }}</pre>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2">Previous Value</label>
                        <div class="col-sm-10">
                            <pre>{{ $item->values()->latest()->first()->previous()->value }}</pre>
                        </div>
                    </div>
                @endisset
                @isset($item->valueHtml)
                    <div class="row">
                        <label class="col-sm-2">HTML</label>
                        <div class="col-sm-10">
                            {!! $item->html !!}
                        </div>
                    </div>
                @endisset
                @isset($item->screenshot)
                <div class="row">
                    <label class="col-sm-2">Screenshot</label>
                    <div class="col-sm-10">
                        <img src="{{ $item->screenshotUrl }}" />
                    </div>
                </div>
                @endisset
            </div>
        </div>
    </div>
@endsection