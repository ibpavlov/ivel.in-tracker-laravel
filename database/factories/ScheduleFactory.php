<?php

use App\Models\Item;
use App\Models\Schedule;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Schedule::class, function (Faker $faker) {
    return [
        'item_id' => factory(Item::class)->create()->id,
        'frequency' => $faker->randomElement(Schedule::FREQUENCIES),
        'offset' => 0
    ];
});
