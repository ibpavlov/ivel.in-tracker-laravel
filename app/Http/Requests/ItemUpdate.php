<?php
/**
 * File for ItemUpdateRequest
 */

namespace App\Http\Requests;


class ItemUpdate extends ItemStore
{
    public function authorize()
    {
        $item = $this->route('item');
        return $item && $this->user()->can('update', $item);
    }

    public function rules()
    {
        return parent::rules();
    }
}