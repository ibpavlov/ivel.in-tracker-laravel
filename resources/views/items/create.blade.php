@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-lg-2">
                @include('items.partial.form')
            </div>
        </div>
    </div>
@endsection