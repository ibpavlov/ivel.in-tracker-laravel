<?php /** @noinspection PhpDynamicAsStaticMethodCallInspection */

/**
 * File for ItemTrack
 */

namespace App\Services;


use App\Models\Item;
use App\Models\Value;
use App\Notifications\NewValueFound;
use Flow\JSONPath\JSONPath;
use Goutte\Client;
use Symfony\Component\BrowserKit\Response;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ItemTrack
 * @package App\Services
 */
class ItemTrack
{

    /**
     * @param Item $item
     * @throws \Exception
     */
    public function track(Item $item)
    {
        if ($item->hasInstructions()) {
            //TODO FOOLOW INSTR
        } else {
            $client = new Client();
            $guzzleClient = new \GuzzleHttp\Client([
                RequestOptions::ALLOW_REDIRECTS  => [
                    'max'             => 1
                ],
                RequestOptions::COOKIES => true,
                RequestOptions::VERIFY => false
            ]);
            $client->setClient($guzzleClient);
            $crawler = $client->request('GET', $item->url);
            /** @var Response $response */
            $response = $client->getResponse();
            //$valueFound = $crawler->filter($item->selector)->extract("_text");
            if(stripos($response->getHeader('Content-Type', true),'application/json') !== false) {
                //JSON
                $content = $client->getResponse()->getContent();
                $data = json_decode($content, true);
                if(isset($item->selector)) {
                    $data = (new JSONPath($data))->find($item->selector)->data();
                }
                $valueFound = Yaml::dump($data);
                $htmlFound = json_encode($data, JSON_PRETTY_PRINT);
            } else {
                //HTML maybe
                if(isset($item->selector)) {
                    $valueFound = join("\n", $crawler->filter($item->selector)->each(function (Crawler $node) {
                        $nodeVal = $node->getNode(0);
                        $nodeVal->normalize();
                        return trim($nodeVal->textContent);
                    }));
                    $htmlFound = join("<br />\n", $crawler->filter($item->selector)->each(function (Crawler $node) {
                        return $node->html();
                    }));
                } else {
                    $content = $client->getResponse()->getContent();
                    $valueFound = strip_tags($content);
                    $htmlFound = $content;
                }
            }
        }
        if (empty($valueFound)) {
            throw new \Exception("Value not found Exception");
        }
        if (!is_string($valueFound)) {
            throw new \Exception("Value found is not a string");
        }
        if (!isset($item->value) || strcmp($valueFound, $item->value) !== 0) {
            /** @var Value $value */
            $value = $item->values()->create(['value' => $valueFound]);
            $item->value = $valueFound;
            $item->valueHtml = $htmlFound ?? "";
            //$path = $item->id.".jpg";
            //$fullPath = Storage::disk('local')->path('') . "public\\" .$path;
            //TODO $item->screenshot = $path;
            $item->save();
            $item->notify(new NewValueFound($value));
            //TODO Browsershot::url($item->url)->windowSize(640, 480)->setScreenshotType('jpeg', 100)->save("test.jpg");
        }
    }

}