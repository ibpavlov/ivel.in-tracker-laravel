@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>{{ __("Items") }}</h2>
                    </div>

                    <div class="panel-bod row mb-2">
                        @foreach ($items as $item)
                            @include('items.partial.singleItemFromList')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection