<?php

use App\Models\Item;
use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Item::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'url' => $faker->url,
        'domain' => $faker->domainName,
        'selector' => 'body > div',
        'instructions' => '',
        'value' => '',
        'title' => $faker->words(2, true)
    ];
});
