<?php

namespace App\Models;

use App\Contracts\DbNotifiable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use McCool\LaravelAutoPresenter\HasPresenter;
use App\Presenters;

/**
 * Class Item
 * @package App\Models
 */
class Item extends Model implements DbNotifiable, HasPresenter
{
    use Traits\DbNotifiable, Scopes\Item, SoftDeletes, Notifiable;

    protected $table = 'items';
    public $timestamps = true;

    protected $dates = [
        'deleted_at'
    ];
    protected $fillable = [
        'url', 'publicUrl', 'domain', 'selector', 'instructions', 'value', 'valueHtml', 'title', 'user_id', 'screenshot'
    ];

    public function getPresenterClass()
    {
        return Presenters\Item::class;
    }

    public function hasInstructions()
    {
        return !empty($this->instructions);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'item_id');
    }

    public function values()
    {
        return $this->hasMany(Value::class, 'item_id');
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification $notification
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->emailTo();
    }


}