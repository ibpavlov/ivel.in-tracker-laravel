{{ bs()->openForm(isset($item) ? 'PUT' : 'POST' , route( isset($item) ? 'item.update' : 'item.store' , [$item ?? null])) }}

{{ bs()->formGroup(bs()->text('title')->value($item->title ?? old('title'))->required())
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Title'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->text('url')->value($item->url ?? old('url'))->required()
        ->attribute('pattern','https?://.+')->attribute('title','Include http:// or https://'))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Url'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->text('publicUrl')->value($item->publicUrl ?? old('publicUrl'))
        ->attribute('pattern','https?://.+')->attribute('title','Include http:// or https://'))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Public Url'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->text('domain')->value($item->domain ?? old('domain'))
        ->attribute('pattern','^((?!http)).*')->attribute('title','Without http'))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Domain'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->textarea('selector')->value($item->selector ?? old('selector')))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Selector'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->textarea('instructions')->value($item->instructions ?? old('instructions')))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Instructions'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->text('value')->readOnly()->value($item->value ?? old('value')))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Value'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->select('frequency',[
    'monthly' => __("Monthly"), 'weekly' => __("Weekly"),
    'daily' => __("Daily"), 'hourly' => __("Hourly"), 'minutely' => __("Minutely"),
], $schedule->frequency ?? null)->required())
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Frequency'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->text('offset')->value($schedule->offset ?? old('offset', 0))->attribute('type','number'))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Offset'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->select('via',['mail' => __("Email")], $notificationChannel->via ?? 'mail')->required())
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Via'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->text('extra')->value($notificationChannel->extra ?? old('extra', Auth::user()->email ?? null)))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('To'), false, ['col-sm-2'])->addClass('row') }}
{{ bs()->formGroup(bs()->text('screenshot')->readOnly()->value($item->screenshot ?? null))
    ->wrapControlIn(bs()->div()->addClass('col-sm-10'))->label(__('Screenshot'), false, ['col-sm-2'])->addClass('row') }}

{{ bs()->button("Submit")->addClass('offset-sm-2') }}

{{ bs()->closeForm() }}